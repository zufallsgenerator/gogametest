package constants

// NumEnemies defines number of enemies to allocate from the beginning.
const NumEnemies int = 10
const MaxEnemiesProTileLine = 1000
