BUILD_DIR=build

all: clean build

.PHONY: clean
clean:
	rm -rf build/

.PHONY: copyassets
copyassets:
	rm -rf build/assets
	mkdir -p build
	cp -R assets build/assets

build: copyassets
	go build -o build/gogridtest

.PHONY: spritesheets
spritesheets:
	free-tex-packer-cli --project assets/gogrid.ftpp --output assets/spritesheets

run: build
	build/gogridtest

.PHONY :test
test:
	go test ./game -v
