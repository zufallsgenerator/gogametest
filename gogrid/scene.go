package gogrid

import (
	"image/color"
	"math"

	"github.com/zufallsgenerator/gogame/constants"
	"github.com/faiface/pixel"
	"golang.org/x/image/colornames"
)

// Scene defines a scene.
type Scene struct {
	target     pixel.Target
	bounds     pixel.Rect
	config     SceneConfig
	scroll     pixel.Vec
	tiles      TileSet
	marker     TilePos
	targetSize pixel.Vec
	// internal helper
	_spriteMap map[int]*SpriteList
}

// NewScene creates a new scene.
func NewScene(target pixel.Target, targetSize pixel.Vec, bounds pixel.Rect, config SceneConfig, tiles TileSet) *Scene {
	s := &Scene{
		target:     target,
		targetSize: targetSize,
		bounds:     bounds,
		config:     config,
		scroll:     pixel.ZV,
		tiles:      tiles,
		marker:     TilePos{-1, -1},
	}
	return s
}

func MaxInt(x, y int) int {
	if x > y {
		return x
	}
	return y
}
func MinInt(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func RoundVec(v pixel.Vec) pixel.Vec {
	return pixel.V(math.Round(v.X), math.Round(v.Y))
}

type SpriteList struct {
	slice          [constants.MaxEnemiesProTileLine]Object
	index          int
	requestedIndex int
}

func NewSpriteList() *SpriteList {
	return &SpriteList{}
}

func (l *SpriteList) append(o Object) *SpriteList {
	if l == nil {
		l = NewSpriteList()
	}
	if l.index < len(l.slice) {
		l.slice[l.index] = o
		l.index++
	} else {
		//fmt.Printf("over capacity %d!\n", l.requestedIndex)
	}
	l.requestedIndex++
	return l
}

func (l SpriteList) asSlice() []Object {
	return l.slice[0:l.index]
}

func (l *SpriteList) reset() {
	l.index = 0
	l.requestedIndex = 0
}

func (s *Scene) getSpriteMap() map[int]*SpriteList {
	if s._spriteMap == nil || DebugConf.AlwaysNewSpriteMap {
		s._spriteMap = make(map[int]*SpriteList)
	} else {
		for k := range s._spriteMap {
			s._spriteMap[k].reset()
		}
	}
	return s._spriteMap
}

// Scroll sets the scrolling position of the scene.
func (s *Scene) Scroll(scroll pixel.Vec) {
	s.scroll = RoundVec(pixel.V(scroll.X-s.bounds.Min.X-float64(s.config.TileWidth), float64(s.config.TileHeight)-scroll.Y))
	//	s.scroll = scroll
}

func (s *Scene) SetMarker(newPos TilePos) {
	s.marker = newPos
}

func (s *Scene) VerticalTilePositionForObject(object Object) int {
	return s.config.GetTilePos(object).Y
}

/*

func (s *Scene) Draw(room *Room, objects []Object) {
	spriteMap := make(map[int][]Object)
	if DebugConf.DrawSprites {
		for _, object := range objects {
			index := s.VerticalTilePositionForObject(object)
			spriteMap[index] = append(spriteMap[index], object)
		}
	}

	spriteOffsetY := s.bounds.Max.Y - s.targetSize.Y + float64(s.config.TileHeight*2)

	yMin := 0
	yMax := room.Height

	xMin := MaxInt((int(s.bounds.Min.X+s.scroll.X) / s.config.TileWidth), 0)
	xMax := MinInt(3+(int(s.bounds.Max.X+s.scroll.X)/s.config.TileWidth), room.Width)

	for y := yMin; y < yMax; y++ {
		for x := xMin; x < xMax; x++ {
			tileID := room.Tiles[y][x]
			tile := s.tiles[tileID]
			if tile.Height > s.config.TileHeight {
				continue
			}
			s.drawTile(x, y, tile)
		}

	}

	// second pass, interleave with sprites
	for y := yMin - 1; y < yMax+1; y++ {
		if y >= yMin && y < yMax {
			for x := xMin; x < xMax; x++ {
				tileID := room.Tiles[y][x]
				tile := s.tiles[tileID]
				if tile.Height <= s.config.TileHeight {
					continue
				}
				s.drawTile(x, y, tile)
			}
		}
		// draw sprites
		if objects, ok := spriteMap[y]; ok {
			for _, object := range objects {
				moved := pixel.V(
					float64(object.GetX())-float64(s.config.TileWidth),
					s.targetSize.Y-float64(object.GetY())+spriteOffsetY)
				//					s.targetSize.Y-float64(object.GetY())-float64(s.config.TileHeight))
				matrix := pixel.IM.Moved(moved).Moved(pixel.ZV.Sub(s.scroll))
				object.Draw(s.target, matrix)
			}
		}

	}

}
*/

func (s *Scene) DrawUnorderdSprites(room *Room, objects []Object) {

	spriteOffsetY := s.bounds.Max.Y - s.targetSize.Y + float64(s.config.TileHeight*2)

	yMin := 0
	yMax := room.Height

	xMin := MaxInt((int(s.bounds.Min.X+s.scroll.X) / s.config.TileWidth), 0)
	xMax := MinInt(3+(int(s.bounds.Max.X+s.scroll.X)/s.config.TileWidth), room.Width)

	for y := yMin; y < yMax; y++ {
		for x := xMin; x < xMax; x++ {
			tileID := room.Tiles[y][x]
			tile := s.tiles[tileID]
			if tile.Height > s.config.TileHeight {
				continue
			}
			s.drawTile(x, y, tile)
		}

	}

	// second pass, interleave with sprites
	for y := yMin - 1; y < yMax+1; y++ {
		if y >= yMin && y < yMax {
			for x := xMin; x < xMax; x++ {
				tileID := room.Tiles[y][x]
				tile := s.tiles[tileID]
				if tile.Height <= s.config.TileHeight {
					continue
				}
				s.drawTile(x, y, tile)
			}
		}
	}
	for _, object := range objects {
		moved := pixel.V(
			float64(object.GetX())-float64(s.config.TileWidth),
			s.targetSize.Y-float64(object.GetY())+spriteOffsetY)
		//					s.targetSize.Y-float64(object.GetY())-float64(s.config.TileHeight))
		matrix := pixel.IM.Moved(moved).Moved(pixel.ZV.Sub(s.scroll))
		object.Draw(s.target, matrix)
	}

}

func (s *Scene) DrawWithFog(room *Room, objects []Object, visited *Room) {
	spriteMap := s.getSpriteMap()
	//	spriteMap := make(map[int][1000]Object)
	if DebugConf.DrawSprites {
		for _, object := range objects {
			index := s.VerticalTilePositionForObject(object)
			spriteMap[index] = spriteMap[index].append(object)
			//		spriteMap[index] = append(spriteMap[index], object)
			//		spriteMap[index]
		}
	}

	spriteOffsetY := s.bounds.Max.Y - s.targetSize.Y + float64(s.config.TileHeight*2)

	yMin := 0
	yMax := room.Height

	xMin := MaxInt((int(s.bounds.Min.X+s.scroll.X) / s.config.TileWidth), 0)
	xMax := MinInt(3+(int(s.bounds.Max.X+s.scroll.X)/s.config.TileWidth), room.Width)

	fogColor := color.RGBA{128, 128, 128, 255}

	for y := yMin; y < yMax; y++ {
		for x := xMin; x < xMax; x++ {
			tileID := room.Tiles[y][x]
			tile := s.tiles[tileID]
			if tile.Height > s.config.TileHeight {
				continue
			}
			if (visited.GetTile(TilePos{X: x, Y: y}) > 0) {
				s.drawTileWithColor(x, y, tile, fogColor)
			} else {
				s.drawTile(x, y, tile)
			}
		}
	}

	// second pass, interleave with sprites
	for y := yMin - 1; y < yMax+1; y++ {
		if y >= yMin && y < yMax {
			for x := xMin; x < xMax; x++ {
				tileID := room.Tiles[y][x]
				tile := s.tiles[tileID]
				if tile.Height <= s.config.TileHeight {
					continue
				}
				if (visited.GetTile(TilePos{X: x, Y: y}) > 0) {
					s.drawTileWithColor(x, y, tile, fogColor)
				} else {
					s.drawTile(x, y, tile)
				}
			}
		}
		// draw sprites
		if objects, ok := spriteMap[y]; ok {
			for _, object := range objects.asSlice() {
				//	for _, object := range objects {
				moved := pixel.V(
					float64(object.GetX())-float64(s.config.TileWidth),
					s.targetSize.Y-float64(object.GetY())+spriteOffsetY)
				//					s.targetSize.Y-float64(object.GetY())-float64(s.config.TileHeight))
				matrix := pixel.IM.Moved(moved).Moved(pixel.ZV.Sub(s.scroll))
				if visited.GetTile(s.config.GetTilePos(object)) > 0 || true {
					object.Draw(s.target, matrix)
				}
			}
		}

	}

}

func (s *Scene) drawTileWithColor(x int, y int, tile Tile, providedColorMask color.Color) {
	tileOffset := pixel.V(
		float64(x*s.config.TileWidth-s.config.TileWidth>>1),
		s.bounds.Max.Y-float64(y*s.config.TileHeight))

	tileBounds := tile.Rect().Moved(tileOffset)
	finalMatrix := pixel.IM.Moved(tileOffset).Moved(pixel.ZV.Sub(s.scroll))
	if !s.bounds.Moved(s.scroll).Intersects(tileBounds) {
		//		tile.DrawColorMask(s.target, finalMatrix, colornames.Black)
		return
	}

	// For a checkered effect
	useColorMask := (((y % 2) + x) % 2) == 1
	colorMask := providedColorMask
	if colorMask == nil {
		colorMask = map[bool]color.Color{true: colornames.Lightgray}[useColorMask]
	}

	if s.marker.X == x && s.marker.Y == y {
		colorMask = colornames.Red
	}

	tile.DrawColorMask(s.target, finalMatrix, colorMask)
}

func (s *Scene) drawTile(x int, y int, tile Tile) {
	tileOffset := pixel.V(
		float64(x*s.config.TileWidth-s.config.TileWidth>>1),
		s.bounds.Max.Y-float64(y*s.config.TileHeight))

	tileBounds := tile.Rect().Moved(tileOffset)
	finalMatrix := pixel.IM.Moved(tileOffset).Moved(pixel.ZV.Sub(s.scroll))
	if !s.bounds.Moved(s.scroll).Intersects(tileBounds) {
		//		tile.DrawColorMask(s.target, finalMatrix, colornames.Black)
		return
	}

	// For a checkered effect
	useColorMask := false //(((y % 2) + x) % 2) == 1
	colorMask := map[bool]color.Color{true: colornames.Lightgray}[useColorMask]

	if s.marker.X == x && s.marker.Y == y {
		colorMask = colornames.Red
	}

	tile.DrawColorMask(s.target, finalMatrix, colorMask)
}
