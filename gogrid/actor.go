package gogrid

import (
	"image/color"
	"math"

	"github.com/faiface/pixel"
)

type Brain interface {
	Update(*Actor, *WorldView)
}

type Actor struct {
	sprite         *pixel.Sprite
	Direction      Direction
	animation      Animation
	frameIndex     int
	frameFlipCount float64
	Pos            pixel.Vec
	Velocity       pixel.Vec
	isPlayer       bool
	markerColor    color.Color
	Brain          Brain
	color          color.Color
}

func (a *Actor) IsPlayer() bool {
	return a.isPlayer
}

func MatrixScaled(matrix pixel.Matrix, scale float64) pixel.Matrix {
	return matrix.Scaled(pixel.V(matrix[4], matrix[5]), scale)
}

func (a *Actor) Draw(t pixel.Target, matrix pixel.Matrix) {
	if currentSprite := a.GetCurrentSprite(); currentSprite != nil {
		scale := a.GetCurrentClip().Scale

		var scaledMatrix pixel.Matrix
		if scale > 1 {
			scaledMatrix = MatrixScaled(matrix, scale)
		} else {
			scaledMatrix = matrix
		}
		currentSprite.DrawColorMask(t, scaledMatrix, a.color)
	} else {
		// use default sprite
		a.sprite.DrawColorMask(t, matrix, a.color)
	}
}

func (a *Actor) GetX() int {
	return int(a.Pos.X)
}
func (a *Actor) GetY() int {
	return int(a.Pos.Y)
}

func (a *Actor) ApplyVelocity() {
	a.Pos.X += a.Velocity.X
	a.Pos.Y += a.Velocity.Y
}

func intAbs(value int) int {
	if value < 0 {
		return -value
	}
	return value
}

func (a *Actor) GetHitBox() pixel.Rect {
	size := 10.0
	return pixel.R(a.Pos.X-size, a.Pos.Y-size, a.Pos.X+size, a.Pos.Y+size)
}

func (a *Actor) Update(worldView *WorldView) {
	if a.Brain != nil {
		a.Brain.Update(a, worldView)
	}

	yVel := a.Velocity.Y
	xVel := a.Velocity.X

	didMove := false

	if !worldView.ProjectionIsBlocking(int(a.Pos.X), int(a.Pos.Y+yVel)) {
		a.Pos.Y += yVel
		didMove = true
	}
	if !worldView.ProjectionIsBlocking(int(a.Pos.X+xVel), int(a.Pos.Y)) {
		a.Pos.X += xVel
		didMove = true
	}
	if didMove {
		a.frameFlipCount += math.Abs(xVel) + math.Abs(yVel)
		if a.frameFlipCount > 10 {
			clipLen := len(a.GetCurrentClip().Frames)
			if clipLen > 0 {
				a.frameIndex = (a.frameIndex + 1) % clipLen
			} else {
				a.frameIndex += 0
			}
			a.frameFlipCount = 0
		}
	}
}

func (a *Actor) GetCurrentClip() *Clip {
	clip := a.animation["walk"][a.Direction]
	return &clip
}

func (a *Actor) GetCurrentSprite() *pixel.Sprite {
	currentClip := a.GetCurrentClip()
	frames := currentClip.Frames
	if len(frames) > 0 {
		a.frameIndex = a.frameIndex % len(frames)
		return &frames[a.frameIndex]
	}
	return nil
}
