package gogrid

import "fmt"

type Room struct {
	Width  int
	Height int
	Name   string
	Tiles  [][]int
}

type TilePos struct {
	X int
	Y int
}

func (p TilePos) Neighbours() []TilePos {
	return []TilePos{
		TilePos{p.X - 1, p.Y - 1},
		TilePos{p.X, p.Y - 1},
		TilePos{p.X + 1, p.Y - 1},
		TilePos{p.X - 1, p.Y},
		TilePos{p.X + 1, p.Y},
		TilePos{p.X - 1, p.Y + 1},
		TilePos{p.X, p.Y + 1},
		TilePos{p.X + 1, p.Y + 1},
	}
}

// Redefine if we support larger rooms - with uint32 as TileKey that can be max 65536x65536
type TileKey uint32

func (k TileKey) String() string {
	return fmt.Sprintf("#[%d,%d]", k&0xffff, k>>16)
}

func (p TilePos) Key() TileKey {
	return TileKey(p.X) + TileKey(p.Y<<16)
}

func NewRoom(width int, height int) *Room {
	room := &Room{
		Width:  width,
		Height: height,
	}
	rows := [][]int{}
	for y := 0; y < height; y++ {
		row := []int{}
		for x := 0; x < width; x++ {
			row = append(row, 0)
		}
		rows = append(rows, row)
	}

	room.Tiles = rows
	return room
}

func (r *Room) GetTile(tilePos TilePos) int {
	if tilePos.Y < 0 || tilePos.X < 0 || tilePos.Y >= r.Height || tilePos.X >= r.Width {
		return 0
	}
	return r.Tiles[tilePos.Y][tilePos.X]
}

func (r *Room) SetTile(tilePos TilePos, tile int) {
	if tilePos.Y < 0 || tilePos.X < 0 || tilePos.Y >= r.Height || tilePos.X >= r.Width {
		return
	}
	r.Tiles[tilePos.Y][tilePos.X] = tile
}
