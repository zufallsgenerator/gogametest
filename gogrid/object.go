package gogrid

import (
	"github.com/faiface/pixel"
)

type Object interface {
	Draw(pixel.Target, pixel.Matrix)
	Update(*WorldView)
	IsPlayer() bool
	GetX() int
	GetY() int
	GetHitBox() pixel.Rect
}
