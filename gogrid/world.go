package gogrid

import (
	"math/rand"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// World keeps state and everything in the world.
type World struct {
	Scroll      pixel.Vec
	Visited     *Room
	Room        *Room
	Tiles       TileSet
	Actors      []Object
	SceneConfig SceneConfig
	Rand        *rand.Rand
}

// GetPlayerObject returns the first object that return true for IsPlayer(), or nil
func (w *World) GetPlayerObject() Object {
	for _, object := range w.Actors {
		if object.IsPlayer() {
			return object
		}
	}
	return nil
}

type PosMap map[TileKey][]Object

func NewPosMap() PosMap {
	return make(map[TileKey][]Object)
}

// makeTileToObjectMap creates a map where the key is tile position (as TileKey => uint32),
// value is a list of objects possibly colliding.
// An object will be in up to 9 positions - the centered one and all surrounding ones.
// Given that objects are less than one tile large, we can always collision detect
// economically, even if we start adding hit boxes.
func (w *World) makeTileToObjectMap() map[TileKey][]Object {
	tileToObjectMap := NewPosMap()
	for _, o := range w.Actors {
		tilePos := w.SceneConfig.GetTilePos(o)
		for _, p := range append(tilePos.Neighbours(), tilePos) {
			key := p.Key()
			tileToObjectMap[key] = append(tileToObjectMap[key], o)
		}
	}
	filteredMap := NewPosMap()
	for k := range tileToObjectMap {
		if len(tileToObjectMap[k]) > 1 {
			filteredMap[k] = tileToObjectMap[k]
		}
	}
	return filteredMap
}

/*
func (w *World) checkCollisions() {
	// Work with indicies of actors
	collisionMap := make(map[string]string)

	tileToObjectMap := w.makeTileToObjectMap()
	for k := range tileToObjectMap {
		if len(tileToObjectMap[k]) < 2 {
			continue
		}
    co
	}
}
*/

func (world *World) Update(win *pixelgl.Window) {
	var tileToObjectMap PosMap
	if DebugConf.CalculateCollisionsGrid {
		tileToObjectMap = world.makeTileToObjectMap()
	}

	worldView := &WorldView{
		player:          world.GetPlayerObject(),
		config:          &world.SceneConfig,
		room:            world.Room,
		tiles:           world.Tiles,
		win:             win,
		tileToObjectMap: tileToObjectMap,
		rand:            world.Rand,
	}

	for _, actor := range world.Actors {
		actor.Update(worldView)
	}

}
