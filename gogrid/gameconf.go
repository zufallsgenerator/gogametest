package gogrid

type gameConf struct {
	BorderSize       float64
	ScrollSpeed      float64
	SpritesheetJson  string
	SpritesheetImage string
}

var GameConf = gameConf{
	BorderSize:       0.0,
	ScrollSpeed:      20.0,
	SpritesheetImage: "assets/spritesheets/gogrid.png",
	SpritesheetJson:  "assets/spritesheets/gogrid.json",
}
