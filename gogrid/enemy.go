package gogrid

import (
	"fmt"
	"image/color"
	"math/rand"
	"time"

	"github.com/faiface/pixel"
	"golang.org/x/image/colornames"
)

type EnemyState int

const (
	Default EnemyState = iota
	Patrol
	PrepareAttack
	Attack
	Recovering
)

var enemtyStateDurations = map[EnemyState]int{
	PrepareAttack: 60,
	Attack:        20,
	Recovering:    100,
}

type Enemy struct {
	state          EnemyState
	tick           int
	attackVelocity pixel.Vec
	targetPos      TilePos
	patrolVelocity pixel.Vec
}

func NewEnemy() *Enemy {
	return &Enemy{}
}

func getRand() *rand.Rand {
	return rand.New(rand.NewSource(time.Now().UnixNano()))
}

func GetRandomDirection(speed float64) Direction {
	v := getRand().Intn(4)

	switch v {
	case 0:
		return North
	case 1:
		return South
	case 2:
		return West
	case 3:
		return East
	}
	panic(fmt.Sprintf("Unexpected value: %d", v))
}

func (e *Enemy) Update(a *Actor, worldView *WorldView) {
	if !DebugConf.DoEnemyUpdate {
		return
	}
	attackSpeed := 10.0

	switch e.state {
	case Default:
		{
			direction := GetRandomDirection(1.0)
			a.Direction = direction
			a.Velocity = DirectionToVec(direction).Scaled(2.0)
			e.state = Patrol
			a.color = nil
		}
	case Patrol:
		{
			// scout for player
			player := worldView.GetPlayer()
			if player != nil && intAbs(player.GetX()-a.GetX()) < 200 && intAbs(player.GetY()-a.GetY()) < 200 && worldView.rand.Intn(50) == 0 {
				e.state = PrepareAttack
				e.patrolVelocity = a.Velocity

				e.targetPos = worldView.config.GetTilePos(player)
				playerPos := pixel.V(float64(player.GetX()), float64(player.GetY()))
				//distance := playerPos.Sub(a.Pos).Len() / attackVelocity
				// prepare attack velocity
				// turn in the right direction
				attackVelocity := playerPos.Sub(a.Pos).Unit()

				a.Direction = DirectionFromVec(attackVelocity, a.Direction)
				// just stand down for a while
				a.Velocity = pixel.ZV
				e.tick = 0
				return
			}
			if worldView.ProjectionIsBlockingVec(a.Pos.Add(a.Velocity)) {
				if getRand().Intn(2) == 0 {
					// Get a new direction
					e.state = Default
					return
				}
				a.Velocity = pixel.ZV.Sub(a.Velocity)
				a.Direction = a.Direction.opposite()
			}
		}
		// just count down
	case PrepareAttack:
		{
			e.tick++
			a.color = color.RGBA{uint8(e.tick * 2), 0, 0, 255}
			if e.tick > enemtyStateDurations[PrepareAttack] {
				// launch attack
				e.tick = 0
				e.state = Attack
				a.color = nil
				player := worldView.GetPlayer()
				playerPos := pixel.V(float64(player.GetX()), float64(player.GetY()))
				e.attackVelocity = playerPos.Sub(a.Pos).Unit().Scaled(attackSpeed)
				a.Velocity = e.attackVelocity
			}
		}
	case Attack:
		{
			e.tick++
			if e.tick > enemtyStateDurations[Attack] {
				// go back to normal
				a.Velocity = pixel.ZV.Sub(a.Velocity).Unit().Scaled(0.1)
				e.state = Recovering
				a.color = colornames.Blue
				e.tick = 0
			}
		}
	case Recovering:
		{
			e.tick++
			if e.tick > enemtyStateDurations[Recovering] {
				e.state = Patrol
				a.Velocity = e.patrolVelocity
				a.Direction = DirectionFromVec(e.patrolVelocity, a.Direction)
				a.color = nil
			}

		}

	}

}
