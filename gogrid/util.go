package gogrid

import (
	"bufio"
	"image"
	"log"
	"os"
	"path"
	"path/filepath"

	"github.com/faiface/pixel"
)

// ToAbsolutePath returns an absolute path relative to the current executable.
func ToAbsolutePath(relative string) string {
	executable, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	return path.Join(executable, relative)
}

// WithLinesFromFile reads a file and hands each line the lineParser function
func WithLinesFromFile(filepath string, lineParser func(string)) (bool, error) {
	f, err := os.Open(filepath)

	if err != nil {
		log.Fatal(err)
		return false, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := scanner.Text()
		lineParser(line)
	}
	return true, nil
}

func LoadPicture(path string) (pixel.Picture, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return pixel.PictureDataFromImage(img), nil
}
