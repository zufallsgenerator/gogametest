package gogrid

import "github.com/faiface/pixel"

type Camera struct {
	bounds pixel.Rect
	target Object
	room   *Room
	config SceneConfig
}

func Minf(x float64, y float64) float64 {
	if x < y {
		return x
	}
	return y
}
func Maxf(x float64, y float64) float64 {
	if x > y {
		return x
	}
	return y
}

func (c *Camera) ScrollToTarget(currentPosition pixel.Vec) pixel.Vec {
	speed := 0.05

	maxY := float64(c.room.Height*c.config.TileHeight) - c.bounds.H()
	maxX := float64(c.room.Width*c.config.TileWidth) - c.bounds.W()

	targetX := Minf(Maxf(float64(c.target.GetX())-c.bounds.W()*0.5, 0.0), maxX)
	targetY := Minf(Maxf(float64(c.target.GetY())-c.bounds.H()*0.5, 0.0), maxY)

	deltaX := (targetX - currentPosition.X) * speed
	deltaY := (targetY - currentPosition.Y) * speed

	return pixel.V(currentPosition.X+deltaX, currentPosition.Y+deltaY)
}
