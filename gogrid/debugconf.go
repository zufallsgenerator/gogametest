package gogrid

type debugConf struct {
	DrawSprites             bool
	AlwaysNewSpriteMap      bool
	DoEnemyUpdate           bool
	CalculateCollisionsGrid bool
	PrintMemUsage           bool
	ExitAfterSeconds        int
}

var DebugConf = debugConf{
	DrawSprites:             true,
	DoEnemyUpdate:           true,
	CalculateCollisionsGrid: true,
	AlwaysNewSpriteMap:      true,
	// ---
	PrintMemUsage:    true,
	ExitAfterSeconds: 0,
}
