package gogrid

import (
	"encoding/json"
	"io/ioutil"

	"github.com/faiface/pixel"
)

func readFile(relativePath string) ([]byte, error) {
	data, err := ioutil.ReadFile(relativePath)
	return data, err
}

type ClipJson struct {
	Comment string   `json:"comment"`
	Frames  []string `json:"frames"`
	Scale   float64  `json:"scale"`
}

type Clip struct {
	Comment string
	Frames  []pixel.Sprite
	Scale   float64
}

type AnimationJson map[string]map[string]ClipJson
type Animation map[string]map[Direction]Clip

func (a AnimationJson) GetFrames(activityName string, direction Direction) []string {
	activity := a[activityName]
	directionName := direction.string()
	clip := activity[directionName]
	return clip.Frames
}

func LoadAnimationJson(path string) AnimationJson {
	data, err := readFile(path)
	if err != nil {
		panic(err)
	}
	animation := AnimationJson{}
	err = json.Unmarshal(data, &animation)
	if err == nil {
		return animation
	}
	panic(err)
}

func LoadAnimation(path string, spriteProvider SpriteProvider) Animation {
	animationJson := LoadAnimationJson(path)
	animation := Animation{}
	for activityKey := range animationJson {
		activityJson := animationJson[activityKey]
		activity := make(map[Direction]Clip)
		for directionKey := range activityJson {
			clipJson := activityJson[directionKey]
			direction := DirectionFromString(directionKey)
			sprites, err := spriteProvider.GetSprites(animationJson.GetFrames(activityKey, direction))
			if err == nil {
				activity[direction] = Clip{
					Frames: sprites,
					Scale:  clipJson.Scale,
				}
			}
		}
		animation[activityKey] = activity

	}
	return animation
}
