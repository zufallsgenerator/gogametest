package gogrid

import (
	"image/color"

	"github.com/faiface/pixel"
)

type Tile struct {
	Width      int
	Height     int
	Picture    pixel.Picture
	Sprite     *pixel.Sprite
	IsBlocking bool
}

// Rect returns the bounds of the tile, anchored (0.5, 1)
func (tile Tile) Rect() pixel.Rect {
	tileWidthHalf := tile.Width >> 1
	return pixel.R(
		float64(-tileWidthHalf),
		float64(0),
		float64(tileWidthHalf),
		float64(tile.Height))
}

func (tile Tile) Draw(t pixel.Target, matrix pixel.Matrix) {
	//	tile.Sprite.Draw(t, matrix.Moved(pixel.V(0.0, float64(tile.Height>>1))))
	tile.Sprite.DrawColorMask(t, matrix.Moved(pixel.V(0.0, float64(tile.Height>>1))), nil)
}

func (tile Tile) DrawColorMask(t pixel.Target, matrix pixel.Matrix, mask color.Color) {
	//	tile.Sprite.Draw(t, matrix.Moved(pixel.V(0.0, float64(tile.Height>>1))))
	tile.Sprite.DrawColorMask(t, matrix.Moved(pixel.V(0.0, float64(tile.Height>>1))), mask)
}

type TileSet = map[int]Tile
