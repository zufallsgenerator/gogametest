package gogrid

type SceneConfig struct {
	TileHeight  int
	TileWidth   int
	TileOffsetY int
}

func (c *SceneConfig) GetTilePosXY(x int, y int) TilePos {
	xPos := x / c.TileWidth
	if x < 0 {
		xPos--
	}
	yAdjusted := y + c.TileOffsetY
	yPos := yAdjusted / c.TileHeight
	if yAdjusted < 0 {
		yPos--
	}

	return TilePos{
		X: xPos,
		Y: yPos,
	}
}

func (c *SceneConfig) GetTilePos(o Object) TilePos {
	return c.GetTilePosXY(o.GetX(), o.GetY())
}
