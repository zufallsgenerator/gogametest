package gogrid

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"
	"path/filepath"

	"github.com/faiface/pixel"
)

type Font struct {
	picture     pixel.Picture
	runeToIndex map[rune]int
	charWidth   int
	charHeight  int
}

// FontManifest is the JSON representation of a Font manifest file
type FontManifest struct {
	Image      string `json:"image"`
	CharWidth  int    `json:"charWidth"`
	CharHeight int    `json:"charHeight"`
	Mapping    string `json:"mapping"`
}

// LoadFont loads a font by the manifest file
func LoadFont(manifestPath string) (*Font, error) {
	data, err := ioutil.ReadFile(manifestPath)
	if err != nil {
		return nil, err
	}
	fontJson := FontManifest{}
	err = json.Unmarshal(data, &fontJson)
	if err != nil {
		return nil, err
	}
	if fontJson.CharWidth <= 0 || fontJson.CharHeight <= 0 {
		return nil, fmt.Errorf("Bad char dimension: width: %d, height: %d", fontJson.CharWidth, fontJson.CharHeight)
	}

	imagePath := ToAbsolutePath(path.Join(filepath.Dir(manifestPath), fontJson.Image))
	return NewFontFromPath(imagePath, fontJson.Mapping, fontJson.CharWidth, fontJson.CharHeight)
}

func NewFontFromPath(imagePath string, mapping string, charWidth int, charHeight int) (*Font, error) {
	picture, err := LoadPicture(imagePath)
	if err != nil {
		return nil, err
	}

	runeToIndex := make(map[rune]int)

	for i, r := range []rune(mapping) {
		runeToIndex[r] = i
	}

	fmt.Printf("runeToIndex %v\n", runeToIndex)

	//	sprite := pixel.NewSprite(picture, picture.Bounds())
	return &Font{picture: picture, runeToIndex: runeToIndex, charWidth: charWidth, charHeight: charHeight}, nil

}

// GetSpriteByRune returns nil if there's no sprite for this rune
func (f *Font) GetSpriteByRune(r rune) *pixel.Sprite {
	if index, ok := f.runeToIndex[r]; ok {
		return f.GetSpriteByIndex(index)
	}
	return nil
}

func (f *Font) GetSpriteByIndex(idx int) *pixel.Sprite {
	// TODO: just set picture bounds, don't allocate new sprite
	return pixel.NewSprite(f.picture, f.getBoundsByIndex(idx))
}

func (f *Font) getBoundsByIndex(idx int) pixel.Rect {
	bounds := f.picture.Bounds()

	charsPerLine := int(bounds.W()) / f.charWidth

	x := idx % charsPerLine
	y := idx / charsPerLine

	spriteBounds := pixel.R(
		float64(x*f.charWidth),
		bounds.Max.Y-float64((y+1)*f.charHeight),
		float64((x+1)*f.charWidth),
		bounds.Max.Y-float64(y*f.charHeight),
	)
	return spriteBounds
}
