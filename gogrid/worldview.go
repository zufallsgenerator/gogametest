package gogrid

import (
	"math/rand"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

type WorldView struct {
	player          Object
	config          *SceneConfig
	room            *Room
	tiles           TileSet
	win             *pixelgl.Window
	tileToObjectMap PosMap
	rand            *rand.Rand
}

func (w *WorldView) GetPlayer() Object {
	return w.player
}

// ProjectionIsBlocking returns true if the projected position is blocking.
func (w *WorldView) ProjectionIsBlocking(x int, y int) bool {
	tilePos := w.config.GetTilePosXY(x, y)
	tileID := w.room.GetTile(tilePos)
	if tileID == 0 {
		return true
	}
	tile := w.tiles[tileID]
	return tile.IsBlocking
}

func (w *WorldView) ProjectionIsBlockingVec(v pixel.Vec) bool {
	return w.ProjectionIsBlocking(int(v.X), int(v.Y))
}

func (w *WorldView) CollidingWith(o Object) []Object {
	positions := w.config.GetTilePos(o).Neighbours()
	collisions := []Object{}
	myHitBox := o.GetHitBox()
	for _, p := range positions {
		k := p.Key()
		if w.tileToObjectMap[k] != nil {
			for _, c := range w.tileToObjectMap[k] {
				if c != o {
					if myHitBox.Intersects(c.GetHitBox()) {
						// TODO: how do we avoid duplicates?
						collisions = append(collisions, c)
					}
				}
			}
		}
	}
	return collisions
}
