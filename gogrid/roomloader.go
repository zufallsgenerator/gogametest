package gogrid

import (
	"fmt"
	"strconv"
	"strings"
)

type roomDict = map[string]string

func loadToStringDict(roomPath string) (roomDict, error) {
	m := make(roomDict)
	WithLinesFromFile(roomPath, func(line string) {
		parts := strings.Split(line, "=")
		if len(parts) != 2 {
			return
		}
		m[parts[0]] = parts[1]
	})

	return m, nil
}

// LoadRoom loads a Room from the legacy format.
func LoadRoom(roomPath string) (*Room, error) {
	m, err := loadToStringDict(ToAbsolutePath(roomPath))

	if err != nil {
		return nil, err
	}

	Name := m["Name"]
	Width, err := strconv.Atoi(m["width"])
	if err != nil {
		return nil, err
	}
	Height, err := strconv.Atoi(m["height"])
	if err != nil {
		return nil, err
	}

	rows := [][]int{}

	for y := 0; y < Height; y++ {
		row := []int{}
		for x := 0; x < Width; x++ {
			tile := getTileWithDefault(m, x, y, 0)
			row = append(row, tile)
		}
		rows = append(rows, row)
	}

	//  rows := []int{}

	return &Room{Name: Name, Width: Width, Height: Height, Tiles: rows}, nil
}

func getTileWithDefault(m roomDict, x int, y int, defaultTile int) int {
	stringTile, ok := m[fmt.Sprintf("%dx%d", x, y)]
	if !ok {
		return defaultTile
	}
	tile, err := strconv.Atoi(stringTile)
	if err != nil {
		return defaultTile
	}
	return tile
}
