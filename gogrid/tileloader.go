package gogrid

import (
	"encoding/json"
	"errors"
	_ "image/png"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/faiface/pixel"
)

// LoadTilesFromFolder loads tiles listed in tileDescriptionPath from imageFolder
func LoadTilesFromFolder(tileDescriptionPath string, imageFolder string) (TileSet, error) {

	m := make(TileSet)
	WithLinesFromFile(tileDescriptionPath, func(line string) {
		parts := strings.Split(line, ";")
		if len(parts) < 3 {
			return
		}
		tileID, err := strconv.Atoi(parts[0])
		if err != nil {
			log.Fatal("Not an integer id:", tileID)
			return
		}
		imagePath := path.Join(imageFolder, parts[1])
		picture, err := LoadPicture(imagePath)
		if err == nil {
			width := int(picture.Bounds().W())
			height := int(picture.Bounds().H())
			sprite := pixel.NewSprite(picture, picture.Bounds())
			m[tileID] = Tile{Picture: picture, Sprite: sprite, Width: width, Height: height}
		} else {
			log.Println("Failed loading picture", imagePath, err)
		}
	})
	return m, nil

}

type Frame struct {
	X      int `json:"x"`
	Y      int `json:"y"`
	Width  int `json:"w"`
	Height int `json:"h"`
}

type SheetEntry struct {
	Frame Frame
}

type Spritesheet struct {
	Frames map[string]SheetEntry `json:"frames"`
}

type SpriteProviderFromSheet struct {
	spritesheet *Spritesheet
	picture     pixel.Picture
}

type SpriteProvider interface {
	GetSprite(string) (*pixel.Sprite, error)
	GetSprites([]string) ([]pixel.Sprite, error)
	GetPicture() pixel.Picture
}

func NewSpriteProviderFromSheet(picturePath string, sheetpath string) (SpriteProvider, error) {
	picture, err := LoadPicture(picturePath)
	if err != nil {
		return nil, err
	}
	spritesheet, err := LoadSpritesheet(sheetpath)
	if err != nil {
		return nil, err
	}
	provider := &SpriteProviderFromSheet{
		picture:     picture,
		spritesheet: spritesheet,
	}
	return provider, nil
}

func LoadSpritesheet(path string) (*Spritesheet, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	data, readErr := ioutil.ReadAll(file)
	if readErr != nil {
		return nil, readErr
	}
	var spritesheet Spritesheet
	jsonErr := json.Unmarshal(data, &spritesheet)
	if jsonErr != nil {
		return nil, jsonErr
	}
	//	fmt.Println("spritesheet", spritesheet.Frames)
	return &spritesheet, nil
}

func LoadTilesFromSpritesheet(spriteProvider SpriteProvider, tileDescriptionPath string, tileNamePrefix string) (TileSet, error) {
	m := make(TileSet)

	WithLinesFromFile(tileDescriptionPath, func(line string) {
		parts := strings.Split(line, ";")
		if len(parts) < 3 {
			return
		}
		tileID, err := strconv.Atoi(parts[0])
		if err != nil {
			log.Fatal("Not an integer id:", tileID)
			return
		}
		blockingNumber, err := strconv.Atoi(parts[2])
		if err != nil {
			log.Fatal("Not an integer blocking:", blockingNumber)
			return
		}
		isBlocking := blockingNumber != 0

		imageKey := path.Join(tileNamePrefix, parts[1])

		sprite, err := spriteProvider.GetSprite(imageKey)
		if sprite == nil {
			log.Println("Failed loading picture", imageKey, err)
		} else {
			width := int(sprite.Frame().W())
			height := -int(sprite.Frame().H())
			m[tileID] = Tile{Picture: spriteProvider.GetPicture(), Sprite: sprite, Width: width, Height: height, IsBlocking: isBlocking}
		}
	})
	return m, nil

}

func (s *SpriteProviderFromSheet) GetSprite(key string) (*pixel.Sprite, error) {
	entry := s.spritesheet.Frames[key]
	if &entry == nil {
		return nil, errors.New("No such frame " + key)
	}
	sheetHeight := int(s.picture.Bounds().H())

	f := entry.Frame
	//			bounds := pixel.R(float64(f.X), float64(sheetHeight-f.Y), float64(f.X+f.Width), float64(sheetHeight-f.Y-f.Height))
	bounds := pixel.R(float64(f.X), float64(sheetHeight-f.Y), float64(f.X+f.Width), float64(sheetHeight-f.Y-f.Height))
	sprite := pixel.NewSprite(s.picture, bounds)
	return sprite, nil
}

func (s *SpriteProviderFromSheet) GetSprites(keys []string) ([]pixel.Sprite, error) {
	sprites := []pixel.Sprite{}
	for _, key := range keys {
		sprite, err := s.GetSprite(key)
		if err == nil {
			sprites = append(sprites, *sprite)
		}
	}
	return sprites, nil
}

func (s *SpriteProviderFromSheet) GetPicture() pixel.Picture {
	return s.picture
}
