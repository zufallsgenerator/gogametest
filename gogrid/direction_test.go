package gogrid

import (
	"fmt"
	"testing"

	"github.com/faiface/pixel"
	"github.com/stretchr/testify/assert"
)

var directionTestCases = []struct {
	oldDirection Direction
	velocity     pixel.Vec
	expected     Direction
	message      string
}{
	{East, pixel.V(0, 3), South, "should turn south"},
	{East, pixel.V(0, -3), North, "should turn north"},
	{South, pixel.V(0, 0), South, "should keep direction the same"},
	{North, pixel.V(0, 0), North, "should keep direction the same"},
}

func TestDirectionFromVec(t *testing.T) {
	for _, tt := range directionTestCases {
		actual := DirectionFromVec(tt.velocity, tt.oldDirection)
		desc := fmt.Sprintf("DirectionFromVec(%s, %v) -> %s (expected: %s)", tt.velocity, tt.oldDirection.string(), actual.string(), tt.expected.string())
		assert.Equal(t, tt.expected.string(), actual.string(), "%s | \"%s\"", desc, tt.message)
	}

}
