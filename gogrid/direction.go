package gogrid

import (
	"github.com/faiface/pixel"
)

type Direction int

const (
	North Direction = iota
	South
	West
	East
	NoDirection
)

func DirectionFromString(name string) Direction {
	switch name {
	case "north":
		return North
	case "south":
		return South
	case "west":
		return West
	case "east":
		return East
	}
	return NoDirection
}

func (direction Direction) string() string {
	switch direction {
	case North:
		return "north"
	case South:
		return "south"
	case West:
		return "west"
	case East:
		return "east"
	}
	return "default"

}

func DirectionToVec(direction Direction) pixel.Vec {
	switch direction {
	case North:
		return pixel.V(0, -1.0)
	case South:
		return pixel.V(0, 1.0)
	case West:
		return pixel.V(-1, 0)
	case East:
		return pixel.V(1, 0)
	}
	panic("Invalid direction")
}

func (direction Direction) opposite() Direction {
	switch direction {
	case North:
		return South
	case South:
		return North
	case West:
		return East
	case East:
		return West
	}
	return direction
}

func DirectionFromVec(velocity pixel.Vec, old Direction) Direction {
	v := velocity.Unit()

	// Check if unnormalized velocity is null. pixel.Vec.Unit() never returns
	// {0, 0} but always normalizes to 1
	if velocity.X == 0 && velocity.Y == 0 {
		return old
	}

	switch old {
	case North:
		if v.Y < -0.5 {
			return North
		}
	case South:
		if v.Y > 0.5 {
			return South
		}
	case West:
		if v.X < -0.5 {
			return West
		}
	case East:
		if v.X > 0.5 {
			return East
		}
	}

	if v.Y >= 0.5 {
		return South
	}
	if v.Y <= -0.5 {
		return North
	}
	if v.X >= 0.5 {
		return East
	}
	if v.X <= -0.5 {
		return West
	}

	return old
}
