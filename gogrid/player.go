package gogrid

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"golang.org/x/image/colornames"
)

type PlayerState int

const (
	PlayerDefault PlayerState = iota
	PlayerHurt
)

type Player struct {
	tick  int
	state PlayerState
}

func (p *Player) Update(a *Actor, worldView *WorldView) {
	playerSpeed := 3.0
	a.Velocity = pixel.ZV
	win := worldView.win

	if win.Pressed(pixelgl.KeyA) {
		a.Velocity.X = -playerSpeed
	}
	if win.Pressed(pixelgl.KeyD) {
		a.Velocity.X = playerSpeed
	}
	if win.Pressed(pixelgl.KeyW) {
		a.Velocity.Y = -playerSpeed
	}
	if win.Pressed(pixelgl.KeyS) {
		a.Velocity.Y = playerSpeed
	}

	newDirection := DirectionFromVec(a.Velocity, a.Direction)
	if newDirection != a.Direction {
		a.Direction = newDirection
	}

	switch p.state {
	case PlayerHurt:
		{
			p.tick++
			if p.tick > 60 {
				p.state = PlayerDefault
				a.color = nil
			} else {
				if (p.tick>>3)&1 == 0 {
					a.color = colornames.Red
				} else {
					a.color = colornames.White
				}
			}
			return
		}
	case PlayerDefault:
		{
			colliding := worldView.CollidingWith(a)
			for _, c := range colliding {
				if !c.IsPlayer() {
					p.tick = 0
					p.state = PlayerHurt
				}
			}
		}
	}

}
