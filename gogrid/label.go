package gogrid

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"golang.org/x/image/colornames"
)

type HorizontalAlignment int
type VerticalAlignment int
type TextAlignment int

const (
	LabelDefaultHorizontalAlignment HorizontalAlignment = iota
	LabelLeft                       HorizontalAlignment = iota
	LabelRight                      HorizontalAlignment = iota
)

const (
	LabelDefaultVerticalAlignment VerticalAlignment = iota
	LabelTop                      VerticalAlignment = iota
	LabelBottom                   VerticalAlignment = iota
)

const (
	TextLeft   TextAlignment = iota
	TextCenter TextAlignment = iota
	TextRight  TextAlignment = iota
)

type Label struct {
	text                string
	font                *Font
	scale               int
	position            pixel.Vec
	bounds              pixel.Rect
	textAlignment       TextAlignment
	horizontalAlignment HorizontalAlignment
	verticalAlignment   VerticalAlignment
	positionIsDirty     bool
}

// NewLabel creates a new label with given text and font
func NewLabel(text string, font *Font, bounds pixel.Rect) *Label {
	return &Label{
		text:     text,
		scale:    1,
		font:     font,
		position: pixel.ZV,
		bounds:   bounds,
	}
}

func (l *Label) recalculatePosition() {
	size := l.GetSize()
	halfCharWidth := float64(l.font.charWidth) * 0.5
	halfCharHeight := float64(l.font.charHeight) * 0.5

	switch l.horizontalAlignment {
	case LabelLeft:
		l.position.X = halfCharWidth
	case LabelRight:
		l.position.X = l.bounds.Max.X - size.X - halfCharWidth
	default:

	}
	switch l.verticalAlignment {
	case LabelTop:
		l.position.Y = l.bounds.Max.Y - size.Y - halfCharHeight
	case LabelBottom:
		l.position.Y = size.Y + halfCharHeight
	default:

	}
	l.positionIsDirty = false
}

func (l *Label) SetHorizontalAlignment(alignment HorizontalAlignment) *Label {
	l.horizontalAlignment = alignment
	l.positionIsDirty = true
	return l
}

func (l *Label) SetVerticalAlignment(alignment VerticalAlignment) *Label {
	l.verticalAlignment = alignment
	l.positionIsDirty = true
	return l
}

func (l *Label) SetPosition(position pixel.Vec) *Label {
	l.position = position
	l.positionIsDirty = true
	return l
}

func (l *Label) SetBounds(bounds pixel.Rect) *Label {
	l.bounds = bounds
	l.positionIsDirty = true
	return l
}

func (l *Label) SetText(text string) *Label {
	l.text = text
	l.positionIsDirty = true
	return l
}

func (l *Label) CalculateWidth() int {
	runes := []rune(l.text)
	return len(runes) * l.font.charWidth * l.scale
}

// GetSize returns the dimensions of the label with current text, font and scale.
func (l *Label) GetSize() pixel.Vec {
	runes := []rune(l.text)
	var numLines int = 1
	for _, r := range runes {
		if r == rune('\n') {
			numLines++
		}
	}

	return pixel.V(float64(l.CalculateWidth()), float64(numLines*l.font.charHeight*l.scale))
}

func (l *Label) Draw(target pixel.Target) {
	if l.positionIsDirty {
		l.recalculatePosition()
	}
	font := l.font
	scale := float64(l.scale)
	batch := pixel.NewBatch(&pixel.TrianglesData{}, font.picture)

	message := l.text

	runes := []rune(message)
	col := 0
	row := 0
	for _, r := range runes {
		if r == rune('\n') {
			col = 0
			row++
			continue
		}
		sprite := font.GetSpriteByRune(r)
		offset := l.position.Add(pixel.V(float64(col*font.charWidth)*scale, float64(-row*font.charHeight)))
		charBounds := pixel.IM.Scaled(pixel.ZV, float64(scale)).Moved(offset)
		if sprite == nil {
			imd := imdraw.New(nil)
			frame := pixel.V(float64(font.charWidth/2), float64(font.charHeight/2))
			imd.Push(offset.Sub(frame))
			imd.Push(offset.Add(frame))
			imd.Rectangle(1)
			imd.Draw(batch)
		} else {
			sprite.DrawColorMask(batch, charBounds.Moved(pixel.V(scale, -scale)), colornames.Black)
			sprite.DrawColorMask(batch, charBounds, colornames.Beige)
		}
		col++
	}
	batch.Draw(target)

}
