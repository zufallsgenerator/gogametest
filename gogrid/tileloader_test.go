package gogrid

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadSpritesheet(t *testing.T) {
	spritesheet, err := LoadSpritesheet("../fixtures/spritesheet.json")
	if err != nil {
		t.Error("Failed loading spritesheet", err)
	}
	entry := spritesheet.Frames["tile1"]
	assert.NotNil(t, entry)
	frame := entry.Frame
	assert.Equal(t, frame.X, 1)
	assert.Equal(t, frame.Y, 2)
	assert.Equal(t, frame.Width, 129)
	assert.Equal(t, frame.Height, 130)
}
