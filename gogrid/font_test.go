package gogrid

import (
	"testing"

	"github.com/faiface/pixel"
	"github.com/stretchr/testify/assert"
)

type mockPicture struct {
	rect pixel.Rect
}

func (p mockPicture) Bounds() pixel.Rect {
	return p.rect
}

func TestGetBoundsByIndex(t *testing.T) {
	font := Font{
		picture:    mockPicture{pixel.R(0, 0, 160, 96)},
		charWidth:  16,
		charHeight: 16,
	}

	// Upper left corner
	assert.Equal(t, pixel.R(0, 80, 16, 96), font.getBoundsByIndex(0))
	// Second column, top line
	assert.Equal(t, pixel.R(16, 80, 32, 96), font.getBoundsByIndex(1))
	// First char, second line
	assert.Equal(t, pixel.R(0, 64, 16, 80), font.getBoundsByIndex(10))
}
