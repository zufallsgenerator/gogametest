package gogrid

import (
	"fmt"
	_ "image/gif" // Enable use to use gif when loading pictures
	"math/rand"
	"runtime"
	"time"

	"github.com/zufallsgenerator/gogame/constants"

	"golang.org/x/image/colornames"

	"github.com/faiface/pixel"

	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
)

func pictureToSprite(pic pixel.Picture) *pixel.Sprite {
	return pixel.NewSprite(pic, pic.Bounds())
}

func picturesToSprite(pictures []pixel.Picture) []*pixel.Sprite {
	sprites := []*pixel.Sprite{}
	for _, picture := range pictures {
		sprites = append(sprites, pictureToSprite(picture))
	}

	return sprites
}

func getDirectionSpritesFromAnimation(spriteProvider SpriteProvider, animationPath string) map[Direction][]pixel.Sprite {
	animation := LoadAnimation(animationPath, spriteProvider)
	if animation == nil {
		panic("Failed loading animation " + animationPath)
	}

	dirSprite := make(map[Direction][]pixel.Sprite)

	dirSprite[North] = animation["walk"][North].Frames
	dirSprite[South] = animation["walk"][South].Frames
	dirSprite[East] = animation["walk"][East].Frames
	dirSprite[West] = animation["walk"][West].Frames
	return dirSprite
}

func makeActors(spriteProvider SpriteProvider) []Object {
	enemySprite, err := spriteProvider.GetSprite("gfx/thief/walking s0001.bmp.gif")
	if err != nil {
		panic(err)
	}

	actors := make([]Object, constants.NumEnemies)
	r := rand.New(rand.NewSource(99))

	for i := 0; i < constants.NumEnemies; i++ {
		actors[i] = &Actor{
			animation: LoadAnimation("assets/animations/blob.json", spriteProvider),
			sprite:    enemySprite,
			Pos:       pixel.V(200+float64(r.Intn(500)), 200+float64(r.Intn(500))),
			Velocity:  pixel.V(1.0-float64(r.Intn(2)), 1.0-float64(r.Intn(2))),
			Brain:     &Enemy{},
		}
	}
	return actors
}

func makePlayer(spriteProvider SpriteProvider) *Actor {
	playerSprite, err := spriteProvider.GetSprite("gfx/player/walking s0001.bmp.gif")
	if err != nil {
		panic(err)
	}

	playerObject := &Actor{
		sprite:    playerSprite,
		animation: LoadAnimation("assets/animations/player.json", spriteProvider),
		Pos:       pixel.V(100, 100),
		isPlayer:  true,
		Brain:     &Player{},
	}

	return playerObject
}

func DrawOverlay(target pixel.Target, bounds pixel.Rect, font *Font) {
	batch := pixel.NewBatch(&pixel.TrianglesData{}, font.picture)

	scale := 1

	message := "A B C Hello, world! Numbers [1-40]åäö"

	runes := []rune(message)
	for i, r := range runes {
		sprite := font.GetSpriteByRune(r)
		offset := pixel.V(float64(20+(i*scale*font.charWidth)), bounds.Max.Y-20)
		charBounds := pixel.IM.Scaled(pixel.ZV, float64(scale)).Moved(offset)
		if sprite == nil {
			imd := imdraw.New(nil)
			frame := pixel.V(float64(font.charWidth/2), float64(font.charHeight/2))
			imd.Push(offset.Sub(frame))
			imd.Push(offset.Add(frame))
			imd.Rectangle(1)
			imd.Draw(batch)
		} else {
			sprite.Draw(batch, charBounds)
		}
	}
	batch.Draw(target)

}

func PositionTopRight(bounds pixel.Rect, size pixel.Vec, offset float64) pixel.Vec {
	return pixel.V(bounds.Max.X-size.X-offset, bounds.Max.Y-size.Y-offset)
}

func Run() {
	cfg := pixelgl.WindowConfig{
		Title:  "Pixel Rocks!",
		Bounds: pixel.R(0, 0, 1024, 768),
		VSync:  true,
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	scrollSpeed := GameConf.ScrollSpeed

	win.SetSmooth(false) // we want pixels!

	spriteProvider, err := NewSpriteProviderFromSheet(GameConf.SpritesheetImage, GameConf.SpritesheetJson)
	if err != nil {
		panic(err)
	}

	tiles, err := LoadTilesFromSpritesheet(spriteProvider, "assets/tiles.txt", "gfx/tiles")
	if err != nil {
		panic(err)
	}

	room, err := LoadRoom("assets/start.room")
	if err != nil {
		panic(err)
	}

	font, err := LoadFont("assets/fonts/mono10/manifest.json")
	if err != nil {
		panic(err)
	}

	fpsLabel := NewLabel("", font, win.Bounds())
	fpsLabel.scale = 2
	fpsLabel.SetVerticalAlignment(LabelTop).SetHorizontalAlignment(LabelRight)

	memLabel := NewLabel("", font, win.Bounds())
	memLabel.SetVerticalAlignment(LabelBottom).SetHorizontalAlignment(LabelLeft)

	seconds := 0

	world := World{
		Scroll:      pixel.V(0, 0),
		Visited:     NewRoom(room.Width, room.Height),
		SceneConfig: SceneConfig{TileWidth: 64, TileHeight: 48, TileOffsetY: -24},
		Room:        room,
		Tiles:       tiles,
		Rand:        rand.New(rand.NewSource(time.Now().UnixNano())),
	}

	world.Actors = append(makeActors(spriteProvider), makePlayer(spriteProvider))

	var (
		frameCount = 0
		second     = time.Tick(time.Second)
	)

	batch := pixel.NewBatch(&pixel.TrianglesData{}, spriteProvider.GetPicture())

	borderSize := GameConf.BorderSize

	bounds := pixel.R(borderSize, borderSize, win.Bounds().Max.X-borderSize, win.Bounds().Max.Y-borderSize)
	scene := NewScene(batch, win.Bounds().Size(), bounds, world.SceneConfig, tiles)

	marker := TilePos{}

	camera := Camera{
		target: world.GetPlayerObject(),
		bounds: bounds,
		room:   room,
		config: world.SceneConfig,
	}

	for !win.Closed() {
		playerObject := world.GetPlayerObject()

		frameCount++
		select {
		case <-second:
			fpsLabel.SetText(fmt.Sprintf("FPS: %d", frameCount))
			win.SetTitle(fmt.Sprintf("TilePos %d,%d, scroll: %d,%d | FPS: %d | Player: %d, %d, room Height: %d tiles, %d px", marker.X, marker.Y, int(world.Scroll.X), int(world.Scroll.Y), frameCount, playerObject.GetX(), playerObject.GetY(), room.Height, room.Height*world.SceneConfig.TileHeight))
			if DebugConf.PrintMemUsage {
				memLabel.SetText(FormatMemUsage())
			}
			seconds++
			if DebugConf.ExitAfterSeconds > 0 && seconds > DebugConf.ExitAfterSeconds {
				win.SetClosed(true)

			}
			frameCount = 0
		default:
		}

		world.Scroll = camera.ScrollToTarget(world.Scroll)

		if win.Pressed(pixelgl.KeyLeftShift) || true {
			// Do update
			world.Update(win)
		}

		{
			// Render
			marker = world.SceneConfig.GetTilePos(playerObject)
			scene.SetMarker(marker)
			scene.Scroll(world.Scroll)

			fog := NewRoom(room.Width, room.Height)

			win.Clear(colornames.Gray)
			objects := make([]Object, len(world.Actors))
			for i, actor := range world.Actors {
				objects[i] = actor
				fog.SetTile(world.SceneConfig.GetTilePos(actor), 1)
			}
			//			scene.Draw(room, objects)
			batch.Clear()
			scene.DrawWithFog(room, objects, fog)
			//scene.DrawUnorderdSprites(room, objects)
			batch.Draw(win)

			imd := imdraw.New(nil)

			imd.Push(bounds.Min)
			imd.Push(bounds.Max)
			imd.Rectangle(1)
			imd.Draw(win)

			fpsLabel.Draw(win)
			memLabel.Draw(win)

			win.Update()

		}

		if win.Pressed(pixelgl.KeyLeft) {
			world.Scroll.X -= scrollSpeed
		}
		if win.Pressed(pixelgl.KeyRight) {
			world.Scroll.X += scrollSpeed
		}
		if win.Pressed(pixelgl.KeyUp) {
			world.Scroll.Y -= scrollSpeed
		}
		if win.Pressed(pixelgl.KeyDown) {
			world.Scroll.Y += scrollSpeed
		}

		/*
			playerPos := sceneConfig.GetTilePos(playerObject)
			world.Visited.SetTile(playerPos, 1)
			for _, pos := range playerPos.Neighbours() {
				world.Visited.SetTile(pos, 1)
			}
		*/
	}
}

func PrintMemUsage() {
	fmt.Println(FormatMemUsage())
}

func FormatMemUsage() string {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats

	return fmt.Sprintf("Alloc = %v MiB\nTotalAlloc = %v MiB\nSys = %v MiB\nNumGC = %v\nStackInUse = %v\n",
		bToMb(m.Alloc), bToMb(m.TotalAlloc), bToMb(m.Sys), m.NumGC, m.StackInuse)

}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
