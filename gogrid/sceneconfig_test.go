package gogrid

import (
	"testing"
)

type in struct {
	x int
	y int
}

var postests = []struct {
	in  in
	out TilePos
}{
	{in{0, 0}, TilePos{0, -1}},
	{in{32, 50}, TilePos{0, 1}},
	{in{130, 50}, TilePos{2, 1}},
	{in{-1, 0}, TilePos{-1, -1}},
}

func TestGetTilePosXY(t *testing.T) {
	tileHeight := 32
	tileWidth := 64
	gameConfig := &SceneConfig{
		TileWidth:   tileWidth,
		TileHeight:  tileHeight,
		TileOffsetY: -12,
	}

	for _, tt := range postests {
		tilePos := gameConfig.GetTilePosXY(tt.in.x, tt.in.y)
		if tilePos != tt.out {
			t.Errorf("gameConfig.GetTilePosXY(%d, %d) => %q, want %q", tt.in.x, tt.in.y, tilePos, tt.out)
		}
	}
}
