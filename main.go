package main

import (
	"github.com/zufallsgenerator/gogame/gogrid"

	"github.com/faiface/pixel/pixelgl"
)

func main() {
	pixelgl.Run(gogrid.Run)
}
